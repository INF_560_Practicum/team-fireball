# INF560 Data Informatics Professional Practicum

* [Project Information](#markdown-header-project-information)
* [Project Objective](#markdown-header-project-objective)
* [Dependencies](#markdown-header-dependencies)
* [System Design](#markdown-header-system-design)
* [Code](#markdown-header-code)
    * [Data Analysis](#markdown-header-data-analysis)
    * [AWS Back-End](#markdown-header-aws-back-end)
    * [Web Front-End](#markdown-header-web-front-end)
* [Data](#markdown-header-data)
* [Demo](#markdown-header-demo)
* [Documnet](#markdown-header-document)


---
## Project Information

Project:   
Predict Restroom Usage Based on Visiting Flows

Team Fireball       
Tian    Yang    4979906415     
Qiandi  Qiu     8657765950     
Zhiqing Chen    4042924898    
Xinyuan Zhou    7668647316    
Zhiyuan Wang    4118607907    

---
## Project Objective

This project is to predict the restrooms' using flows in order to help both passengers and the airport staff to better manage their time. We build a web application to provide suggestions to both passengers and airport cleaning staffs about the restrooms’ usage, to help passengers avoiding congestion and optimize their experience in Rio Airport.

---
## Dependencies

### Python
Pandas   
Keras   
Tensorflow   
Sklearn

### Web    
D3.js      
bootstrap     
Google Maps JavaScript API     

### AWS    
Lambda       
API Gateway      
DynamoDB     
S3

---
## System Design
![](Figure/SystemDesign.png)


---
## Code

### Data Analysis

#### [Data_preprocessing.ipynb](Data_Analysis/Data_preprocessing.ipynb)
The data is from Kiana, Rio Airport, and the floor that we chose is Terminal2, Level2. The location (longitude and altitude) of 8 restrooms are defined by Google Map and InmapZ. First, we use Spark quickly output the data around each restroom. And then using DBScan to reduce the duplicate, including similar movement of passenger which means a passenger may carry more than one electronic device with them.

#### [LocationX.ipynb](Data_Analysis/)
After data preprocessing part, we conducted data analysis on 8 data files respectively. The analysis included displaying the data modeling and data evaluation. The data is in format of times series within time period of 10 minutes. And then we use three models RNN, LSTM and GRU models. After applying the three models, we compared the RMSE results of the three models, and selected the best prediction result which had the lowest RMSE. Finally, we applied LSTM on the data and tuned the parameters, and output the predicted data into files.


### AWS Back-End

#### [login.py](AWS/login.py)
Verify account information

#### [getCurrentCount.py](AWS/getCurrentCount.py)
Get people count of 8 restroom by the model files in S3. This function works for dashboard page.

#### [getCount.py](AWS/getCount.py)
Get people count of 1 restroom by the model files in S3. This function works for suggestion page.

#### [uploadTimeProfile.py](AWS/uploadTimeProfile.py)
Upload work time of user to dynamodb table "TimeProfile".

#### [getTimeProfile.py](AWS/getTimeProfile.py)
Get time profile from dynamodb table "TimeProfile".

#### Deployment
Import functions by the corresponding .yam files on AWS Lambda.

### Web Front-End

#### [login.html](Web/login.html)
Staff Login Page         
Enter username and passward     
#### [index.html](Web/index.html)
Heatmap for all users
#### [index2.html](Web/index2.html)
Heatmap for staff
#### [profile.html](Web/profile.html)
Check Working Time Table 
#### [update.html](Web/update.html)
Edit Working Time Table
#### [suggestion.html](Web/suggestion.html)
Restroom usage linechart and best cleaning time suggestion based on working time table
#### Web Deployment
Bootstrap: npm install bootstrap     
Start Web Server: http-server . -p 8000 

---
## Data
### Data description: 
We use data provided by Kiana from Rio Airport. The data is in terminal 2, Level 2, from 2019-08-01 to 2019-12-31.

### [Location data](Data/location data/)
Data preprocessing output which find the data around each restroom.

### [Count data](Data/count_data/)
The count of passengers of each restroom in every ten minutes.

### [Prediction data](Data/predict/)
Predict the count of people that each restrooms may have every 10 minutes under LSTM model.

---
## Demo

### Staff Login

Enter username and passward 

![](Figure/login.png)

### Staff Working Time Table 

Users can see the working time table in profile-preview page.

![](Figure/worktime1.png)

Users can click the checkbox to update their working time table.

![](Figure/worktime2.png)

### Suggestion System
Users can choose restroom number and date to check the restroom count line chart. And system will give the best cleaning time suggestion based on working time table.

![](Figure/suggestion.png)

### Heatmap
User can drag the human icon to choose the location and input times to update the heatmap.

![](Figure/heatmap.png)

---
## Document

### [Final Report](Document/Final_Report.pdf)

### [Final Presentation](Document/Final_Report.pdf)

---

