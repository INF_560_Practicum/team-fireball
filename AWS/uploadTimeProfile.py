import json
import boto3


def lambda_handler(event, context):
    # TODO implement

    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('profile')
    username = event['username']
    time = event['time']
    try:
        table.put_item(
            Item={
                'username': username,
                'time': time
            }
        )
    except Exception:
        traceback.print_exc()
    return 'True'
