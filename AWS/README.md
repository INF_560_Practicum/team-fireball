# AWS
Writen by: Zhiyuan Wang (wang723@usc.edu)

## Code

### login.py
Verify account information
### getCurrentCount.py
Get people count of 8 restroom by the model files in S3. This function works for dashboard page.
### getCount.py 
Get people count of 1 restroom by the model files in S3. This function works for suggestion page.
### uploadTimeProfile.py 
Upload work time of user to dynamodb table "TimeProfile".
### getTimeProfile.py
Get time profile from dynamodb table "TimeProfile".

## Deployment
Import functions by the corresponding .yam files on AWS Lambda.

## Model file
### data
Original data provided by Kiana.
### predict
Predicted data provided by LSTM.
