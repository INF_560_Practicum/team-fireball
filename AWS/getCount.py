import json
import boto3
import csv
import string
import datetime

s3 = boto3.client('s3')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('profile')


def lambda_handler(event, context):
    # TODO implement

    num = event['num']
    date = event['date']
    username = event['username']
    response = table.get_item(
        Key={
            'username': username
        }
    )
    objectName = 'data' + num + '.csv'
    bucketName = 'datafireball'
    s3.download_file(bucketName, objectName, '/tmp/' + objectName)
    return getinfo(num, date, response['Item']['time'])


def getinfo(num, date, response):
    filename = '/tmp/' + "data" + str(num) + ".csv";
    # print(filename)
    file = open(filename, "r")
    reader = csv.reader(file)

    info = []
    result = []
    for i in reader:
        if reader.line_num == 1:
            continue
        res = i[0].find(date)
        if res != -1:
            info.append([i[0], i[1]])
            result.append(i[1])
    file.close()

    year, month, day = (int(x) for x in date.split('-'))
    weekday = datetime.date(year, month, day).weekday()
    worktime = []
    for i in range(8):
        worktime.append(response[i * 7 + weekday])
    suggestiontime = []
    for i in range(8):
        if worktime[i] == '1':
            hour = result[48 + i * 12: 60 + i * 12]
            suggestiontime.append(hour.index(min(hour)) + 48 + i * 12)
    time = ""
    print(suggestiontime)
    for i in suggestiontime:
        time = time + "&" + info[i][0]
    return result, time



