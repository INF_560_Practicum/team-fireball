import json
import boto3
import csv
import string
import datetime

s3 = boto3.client('s3')


def lambda_handler(event, context):
    # TODO implement

    date = event['date']
    bucketName = 'datafireball'
    count = ''
    for num in range(1, 9):
        objectName = 'data' + str(num) + '.csv'
        s3.download_file(bucketName, objectName, '/tmp/' + objectName)
        count += getinfo(num, date)
    return count


def getinfo(num, date):
    filename = '/tmp/' + "data" + str(num) + ".csv";
    # print(filename)
    file = open(filename, "r")
    reader = csv.reader(file)

    for i in reader:
        if reader.line_num == 1:
            continue
        res = i[0].find(date)
        if res != -1:
            result = i[1]
    file.close()

    return result



