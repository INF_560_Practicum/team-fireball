import json
import boto3


def lambda_handler(event, context):
    # TODO implement

    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('account')
    response = table.get_item(
        Key={
            'username': event['username']
        })
    if 'Item' not in response:
        return 'False'
    if response['Item']['password'].strip() != event['password']:
        return 'False'
    return 'True'

